package com.epam;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static java.lang.Boolean.valueOf;

 class GoogleResult {

    private static Logger logger = LogManager.getLogger(GoogleResult.class);

    private WebDriver driver;

    @FindBy(xpath = "//div[@role='tab'][2]/a")
    private WebElement imagesElement;

    @FindBy(xpath = "//div[@role='tab'][2]")
    private WebElement image;

     GoogleResult(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

     WebElement getImagesElement() {
        imagesElement.click();
        logger.info("Click to show images");
        return imagesElement;
    }

     boolean isImageActive() {
        return valueOf(image.getAttribute("aria-selected"));
    }

}
