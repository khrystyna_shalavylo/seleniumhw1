package com.epam;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

class GoogleSearch {

    private WebDriver driver;
    private static Logger logger = LogManager.getLogger(GoogleSearch.class);


    @FindBy(name = "q")
    private WebElement searchElement;

     GoogleSearch(WebDriver driver){
        this.driver=driver;
         PageFactory.initElements(driver, this);
    }


     void setKey(String key) {
        searchElement.sendKeys(key);
        logger.info("Typing text: " + key);
        searchElement.submit();
        logger.info("Click submit button");
    }
}
