package com.epam;

public class Constants {
    public static final String WEBDRIVER = "webdriver.chrome.driver";
    public static final String WEBDRIVER_PATH = "src/main/resources/chromedriver.exe";
    public static final String URL = "https://www.google.com.ua";
}
