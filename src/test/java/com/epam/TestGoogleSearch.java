package com.epam;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

import static com.epam.Constants.URL;
import static com.epam.Constants.WEBDRIVER;
import static com.epam.Constants.WEBDRIVER_PATH;

    public class TestGoogleSearch {
        private WebDriver driver;
        private GoogleSearch googleSearch;
        private GoogleResult googleResult;

        @BeforeClass
        public void setUp() {
            System.setProperty(WEBDRIVER, WEBDRIVER_PATH);
            driver = new ChromeDriver();
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            googleResult = new GoogleResult(driver);
            googleSearch = new GoogleSearch(driver);
        }

        @Test
        public void testSearchAndShowImagesResult() {
            driver.get(URL);
            googleSearch.setKey("Apple");
            googleResult.getImagesElement();
            Assert.assertTrue(googleResult.isImageActive(), "Image is not opened");
        }

        @AfterClass
        public void shutDown() {
            driver.quit();
        }
    }
